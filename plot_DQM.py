# -*- coding: utf-8 -*-

"""
Script to plot a single efficiency histogram from the DQM.
"""

import os
import numpy
import argparse
import uproot
import matplotlib
import matplotlib.pyplot
import mplhep

import scipy.stats

matplotlib.use('Agg')
matplotlib.pyplot.style.use(mplhep.style.CMS)



## function to calculate the asymmetric error for the trigger efficiency
# using Clopper-Pearson interval like defined in
# https://de.wikipedia.org/wiki/Konfidenzintervall_f%C3%BCr_die_Erfolgswahrscheinlichkeit_der_Binomialverteilung
# @param k      number of hits
# @param n      number of experiments
# @param gamma  (optional) confidence level for interval; default is one sigma (68,2%)
#
# @retval list  containing the lower and upper error
def getError(k, n, gamma=0.682):
    n[n==0] = 1E-8
    alpha = 0.5 * (1-gamma)
    lower = k/n - scipy.stats.beta.ppf(alpha, k, n-k+1)
    upper = scipy.stats.beta.ppf(1-alpha, k+1, n-k) - k/n

    lower[k==0] = 0
    upper[k==n] = 0

    return [lower, upper]


def loadEfficiency(path, nomlabel, denomlabel):
    nums = []
    denoms = []

    with uproot.open(path) as infile:
        infile = infile['DQMData']

        for run in infile.keys(recursive=False):
            numerator = infile[run]['HLT/Run summary'][nomlabel]
            denominator = infile[run]['HLT/Run summary'][denomlabel]

            nums.append(numerator.to_numpy())
            denoms.append(denominator.to_numpy())

    bins = nums[0][1]
    nums = numpy.sum(numpy.array(nums, dtype=object).T[0])
    denoms = numpy.sum(numpy.array(denoms, dtype=object).T[0])
    eff = nums/denoms
    totalEff = numpy.sum(nums)/numpy.sum(denoms)

    return (eff, bins), getError(nums, denoms), totalEff


def plot(path):
    fig = matplotlib.pyplot.figure(figsize=(12, 14))
    gs = matplotlib.gridspec.GridSpec(2, 1, height_ratios=[4, 1])
    plot = fig.add_subplot(gs[0])
    rplot = fig.add_subplot(gs[1], sharex=plot)

    rlabel = f'{args.energy}'
    if args.lumi:
        rlabel = f'${args.lumi}\,$fb$^{{-1}}$ (' + rlabel + ')'

    mplhep.cms.label(loc=1, ax=plot, data=True, paper=False, rlabel=rlabel)

    # efficiency plot
    run3_hist, run3_err, run3_eff = loadEfficiency(path=args.path, nomlabel=args.numerator, denomlabel=args.denominator)
    ref_hist, ref_err, ref_eff = loadEfficiency(path=args.refpath, nomlabel=args.numerator, denomlabel=args.denominator)

    mplhep.histplot([ref_hist, run3_hist],
                    yerr=[ref_err, run3_err],
                    ax=plot,
                    histtype='errorbar',
                    color=['b', 'k',],
                    marker=['_', '.',],
                    label=[f'reference\ntotal: {100 * ref_eff:.1f}%',
                           f'target\ntotal: {100 * run3_eff:.1f}%'])

    print()

    rplot.set_xlabel(args.label, x=1.0, ha='right')
    plot.set_ylabel('efficiency', verticalalignment='bottom', y=1.0,ha='right')
    plot.set_ylim(0, 1.2)
    plot.legend(ncol=2)


    # ratio plot

    ratio = run3_hist[0] / ref_hist[0]

    mplhep.histplot((ratio, run3_hist[1]),
                    ax=rplot,
                    yerr=False,
                    histtype='errorbar',
                    color='k',
                    label='ratio')

    rplot.hlines(1, run3_hist[1][0], run3_hist[1][-1], colors='k', lw=0.5)
    rplot.set_xlim(run3_hist[1][0], run3_hist[1][-1])
    rplot.set_ylabel('target/ref', y=1.0, ha='right')
    rplot.set_ylim(0., 3.)


    fig.tight_layout()
    os.makedirs(os.path.dirname(args.out), exist_ok = True)
    fig.savefig(args.out, dpi=300)
    #matplotlib.pyplot.show()
    matplotlib.pyplot.close()



if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--path', type=str, default='data/BTagMu_Run2022C-PromptReco-v1_fill8033.root',
                        help='path to data files')

    parser.add_argument('--refpath', type=str, default='data/DQM_Offline_Run2018_BTagMu_R0003200xx_S0005.root',
                        help='path to reference data files')

    parser.add_argument('--lumi', type=str, default=None,
                        help='luminosity')

    parser.add_argument('--out', type=str, default='plots/BTagMu_AK4Jet300_Mu5-bJetPt.pdf',
                        help='output name')

    parser.add_argument('--numerator', type=str, default='BTV/BTagMu_Jet/BTagMu_AK4Jet300_Mu5/bjetPt_1_numerator',
                        help='name of the numerator')

    parser.add_argument('--denominator', type=str, default='BTV/BTagMu_Jet/BTagMu_AK4Jet300_Mu5/bjetPt_1_denominator',
                        help='name of the denominator')

    parser.add_argument('--label', type=str, default='b jet pt',
                        help='x label')

    parser.add_argument('--energy', type=str, default='$13.6\,$TeV',
                        help='center of mass energy')

    parser.add_argument('--efficiency', action='store_true',
                        help='include efficiency in legend')

    args = parser.parse_args()
    print(args)

    plot(args)
