import awkward as ak
from coffea.nanoevents.methods import candidate


def setup_deepJet_jets(flat_jets, skip_keys=None):
    if skip_keys is None:
        skip_keys = []
    features = [
        "isB",
        "isBB",
        "isGBB",
        "isLeptonicB",
        "isLeptonicB_C",
        "isC",
        "isGCC",
        "isCC",
        "isUD",
        "isS",
        "isG",
        "isTau",
        "isUndefined",
        "jet_pt",
        "jet_phi",
        "jet_mass",
        "jet_eta",
        "nCpfcan",
        "nNpfcan",
        "nsv",
        "npv",
        "TagVarCSV_trackSumJetEtRatio",
        "TagVarCSV_trackSumJetDeltaR",
        "TagVarCSV_vertexCategory",
        "TagVarCSV_trackSip2dValAboveCharm",
        "TagVarCSV_trackSip2dSigAboveCharm",
        "TagVarCSV_trackSip3dValAboveCharm",
        "TagVarCSV_trackSip3dSigAboveCharm",
        "TagVarCSV_jetNSelectedTracks",
        "TagVarCSV_jetNTracksEtaRel",
        "Cpfcan_BtagPf_trackEtaRel",
        "Cpfcan_BtagPf_trackPtRel",
        "Cpfcan_BtagPf_trackPPar",
        "Cpfcan_BtagPf_trackDeltaR",
        "Cpfcan_BtagPf_trackPParRatio",
        "Cpfcan_BtagPf_trackSip2dVal",
        "Cpfcan_BtagPf_trackSip2dSig",
        "Cpfcan_BtagPf_trackSip3dVal",
        "Cpfcan_BtagPf_trackSip3dSig",
        "Cpfcan_BtagPf_trackJetDistVal",
        "Cpfcan_ptrel",
        "Cpfcan_drminsv",
        "Cpfcan_VTX_ass",
        "Cpfcan_puppiw",
        "Cpfcan_chi2",
        "Cpfcan_quality",
        "Npfcan_ptrel",
        "Npfcan_deltaR",
        "Npfcan_isGamma",
        "Npfcan_HadFrac",
        "Npfcan_drminsv",
        "Npfcan_puppiw",
        "sv_pt",
        "sv_deltaR",
        "sv_mass",
        "sv_ntracks",
        "sv_chi2",
        "sv_normchi2",
        "sv_dxy",
        "sv_dxysig",
        "sv_d3d",
        "sv_d3dsig",
        "sv_costhetasvpv",
        "sv_enratio",
    ]
    feature_dict = {
        feature_name: getattr(flat_jets, feature_name, None)
        for feature_name in features
        if not (feature_name in skip_keys)
    }
    jets = ak.zip(
        {
            **feature_dict,
            "category": ak.zip(
                {
                    "Jet_category_all": flat_jets.jet_pt > -1,
                    "Jet_category_light": flat_jets.isUD + flat_jets.isS + flat_jets.isG,
                    "Jet_category_C": flat_jets.isC + flat_jets.isGCC + flat_jets.isCC,
                    "Jet_category_B": flat_jets.isB
                    + flat_jets.isBB
                    + flat_jets.isGBB
                    + flat_jets.isLeptonicB
                    + flat_jets.isLeptonicB_C,
                }
            ),
            # "hltPFDeepFlavourJetTags_probb": flat_jets.hltPFDeepFlavourJetTags_probb,
            # "hltPFDeepFlavourJetTags_probbb": flat_jets.hltPFDeepFlavourJetTags_probbb,
            # "hltPFDeepFlavourJetTags_problepb": flat_jets.hltPFDeepFlavourJetTags_problepb,
            # "hltPFDeepFlavourJetTags_probc": flat_jets.hltPFDeepFlavourJetTags_probc,
            # "hltPFDeepFlavourJetTags_probuds": flat_jets.hltPFDeepFlavourJetTags_probuds,
            # "hltPFDeepFlavourJetTags_probg": flat_jets.hltPFDeepFlavourJetTags_probg,
        },
        with_name="PtEtaPhiMCandidate",
        behavior=candidate.behavior,
        depth_limit=1,
    )
    return jets
