import awkward as ak

def ttbar_selection(events) -> ak.Array:
    """
    returns a mask with a ttbar selection for a b-enriched phase space
    """
    jetMask = (events.Jet.pt > 30.0) & (abs(events.Jet.eta) < 2.5)

    electronMask = (events.Electron.pt >= 10) \
                    & (abs(events.Electron.eta) <= 2.5) \
                    & (events.Electron.dz < 0.2) \
                    & (events.Electron.dxy < 0.1)

    muonMask = (events.Muon.pt >= 10) \
                & (abs(events.Muon.eta) <= 2.5) \
                & (events.Muon.dz < 0.2) \
                & (events.Muon.dxy < 0.1)

    ## ttbar selection
    ttbarSelection = (ak.sum(jetMask, axis=-1) >= 2) & (ak.sum(electronMask, axis=-1) == 1) & (ak.sum(muonMask, axis=-1) == 1)

    ## oposite charge mask & dilepton mass cut
    dilep = ak.mask(events.Electron[electronMask], ttbarSelection)[:, 0] + ak.mask(events.Muon[muonMask], ttbarSelection)[:, 0]
    dilepMask = (dilep.charge == 0) & (dilep.mass > 20)

    return ak.fill_none(ttbarSelection & dilepMask, False).to_numpy()